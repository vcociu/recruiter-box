Demo: http://www.maine.gov/mdot/jobs/dev/recruiterbox-v5.html 

#How to use:

Styles: the styles are the same as in previous version just added styles for the search box.
##HTML: 
```html
<form id="search-box" action method="POST" class="styled">
    <fieldset style="background: #e8f2fc; margin-bottom: 2%;">
         <!-- Accessibility -->
        <legend class="hidden">Search Jobs</legend>       
        <h4>Search Jobs</h4>
        <p>Enter a search term in the field below to filter the list. The list below will filter as you type.</p>
        <label for="search-crt" class="hidden">Search criteria</label>
        <input type="text" id="search-crt" value="" placeholder="Search" aria-placeholder="Augusta or Midcoast or Seasonal">        
        <p>
            <label for="clear-search" class="hidden">Clear criteria</label>
            <button id="clear-search">Clear Search</button>
        </p>
        <span id="count-jobs">
        </span>
    </fieldset>
</form>

<img id="spinner" src="ajax-loader.gif" alt="spinner" aria-hidden="true">

<div id="jobs"></div>
```

##JavaScript
The only piece of code to be changed is at the bottom of the recruiterbox.js file.
```javascript
/**************
 * RUN
 * Fetch and append the jobs
 **************/
(function () {
    //run recruiterBox
    recruiterBox("MaineDOT", "#jobs");  HERE

    //init search (only once)!
    initializeSearch();
})();
recruiterBox("MaineDOT", "#jobs"); 
```

1. First argument = Agency Tag ex: MaineDOT, DPS, DHHS, BHR …
2. Second argument = div id where the jobs will be added.
>>>
!(OPTIONAL) In the case when you have multiple sections on the website like http://www.maine.gov/nrsc/jobs/index.shtml here you can use the same function to append the jobs based on different tags.
recruiterBox("ACF", "# acfjobs ");
recruiterBox("ACF ", "#depjobs ");
recruiterBox("ACF ", "#ifwjobs ");
recruiterBox("ACF ", "#dmrjobs");
recruiterBox("ACF ", "#nrscjobs");
	
//init search (only once)!
initializeSearch();  this function should be executed once and the latest
>>>

##Use cases:
```javascript
1.	recruiterBox("DHHS", "#dhhsjobs ");// with <div  id=” dhhsjobs”></div> for DHHS
2.	recruiterBox("MaineDOT", "#mainedotjobs "); //with <div  id=”mainedotjobs”></div> for MaineDOT
3.	recruiterBox("DPS", "# jobs"); //with <div  id=”jobs”></div> for DPS
4.	```