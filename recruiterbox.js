//_.js debounce
(function (window) {
    var $ = window.jQuery,
        debounce = function (func, wait, immediate) {
            var timeout, result;
            return function () {
                var context = this,
                    args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) result = func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) result = func.apply(context, args);
                return result;
            };
        };

    $.debounce = debounce;
})(this);

$(document).ready(function () {
    "use strict";
    //vars
    var $spinner = $('#spinner');

    /**
     * fetch and append
     * @param {String} agencyTag set agency tag
     * @param {String} jobsDisplay set the HTML #id tag where the jobs will be added
     */
    var recruiterBox = function (agencyTag, jobsDisplay) {

        var $jQs = $ || jQuery; //get jQuery
        var $jobsDisplay = jobsDisplay ? $(jobsDisplay) : $('#jobs'); // #jobs will be default
        var agencyQueryTag = agencyTag ? agencyTag : ""; //agency query tag
        var template = []; //storage for template        
        //API call
        $jQs.ajax({
            type: "GET",
            url: "https://jsapi.recruiterbox.com/v1/openings/",
            dataType: "json",
            async: 'false',
            data: {
                'client_name': 'mainebhr',
                'tags': agencyQueryTag,
                'limit': 250
            },
            success: function (response) {

                $.each(response.objects, function (key, value) {

                    var remoteLabel = value.allows_remote ? 'Yes' : 'No';

                    var positionTypeLabel = '';

                    if (value.position_type === 'full_time') {
                        positionTypeLabel = "Full Time";
                    } else if (value.position_type === 'part_time') {
                        positionTypeLabel = "Part Time";
                    } else {
                        positionTypeLabel = "Contract";
                    }
                    //append to template array
                    template.push(
                        '<div class="section group article"><div class="joblisting">' +
                        '<h3>' + value.title + '</h3>' +
                        '<div class="section group">' +
                        '<div class="col span_2_of_6">' +
                        '<p><strong>Location:</strong>&nbsp;' + value.location.city + ', ' + value.location.state + ' ' + value.location.zipcode + ' ' + '</p>' +
                        '<p><strong>Job Category:</strong>&nbsp;' + value.team + '</p>' +
                        '</div>' +
                        '<div class="col span_2_of_6">' +
                        '<p><strong>Position Type:</strong>&nbsp;' + positionTypeLabel + '</p>' +
                        '<p><strong>Work Remotely:</strong>&nbsp;' + remoteLabel + '</p>' +
                        '</div>' +
                        '<div class="col span_2_of_6" style="margin:0px">' +
                        '<p><a href="' + value.hosted_url + '" class="button" target=_blank">Apply Now!</a></p>' +
                        '</div>' +
                        '</div>' +
                        '</div></div>'
                    );

                });
                $jobsDisplay.append(template.join(' ')); //append to html
            },
            complete: function () {
                $spinner.hide(400);
            },
            error: function () {
                $spinner.hide(400);
                $jobsDisplay.append("<p>Oops! There seems to be an problem. Please try refreshing the page. If this error persists, please contact our Webmaster.</p>");
            }
        });

    };

    //search 
    var performSearch = function (filter) {
        var countJobsFound = 0;

        //search settings 
        var sanitize = function (string) {
            return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        };
        $spinner.show(400);
        $('.joblisting').hide(400);

        $('.joblisting').each(function () {
            if ($(this).text().search(new RegExp(sanitize(filter), 'i')) < 0) {
                $(this).fadeOut(800);
            } else {
                $(this).show(800);
                countJobsFound++;
            }
        });
        $('#count-jobs').text('Total jobs found: ' + countJobsFound);
        $spinner.hide(400);
    };

    /**
     * Search init !should run once
     */
    var initializeSearch = function () {
        var $searchInput = $('#search-crt'),
            $clearButton = $('#clear-search');

        $searchInput.on('keyup', $.debounce(function (ev) {
            ev.preventDefault();
            var searchCriteria = $(this).val();
            performSearch(searchCriteria);
        }, 500, false));

        $clearButton.on('click', function (ev) {
            ev.preventDefault();
            if ($searchInput.val().length > 0) {
                $searchInput.val("");
                performSearch('');
            }

        });

        $('#search-box').submit(function (ev) {
            ev.preventDefault();
        });
    };

    /**************
     * RUN
     * Fetch and append the jobs
     **************/
    (function () {
        //run recruiterBox
        recruiterBox("MaineDOT", "#jobs");

        //init search (only once)!
        initializeSearch();
    })();

});